<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>
<div class="row">
    <div id="product-<?php the_ID(); ?>" <?php wc_product_class('product-detail-box col-12', $product); ?>>

        <?php
        /**
         * Hook: woocommerce_before_single_product_summary.
         *
         * @hooked woocommerce_show_product_sale_flash - 10
         * @hooked woocommerce_show_product_images - 20
         */
        do_action('woocommerce_before_single_product_summary');
        ?>
        <div>
            <div class="summary entry-summary col-sm-12 col-md-12 col-lg-6">
                <?php
                /**
                 * Hook: woocommerce_single_product_summary.
                 *
                 * @hooked woocommerce_template_single_title - 5
                 * @hooked woocommerce_template_single_rating - 10
                 * @hooked woocommerce_template_single_price - 10
                 * @hooked woocommerce_template_single_excerpt - 20
                 * @hooked woocommerce_template_single_add_to_cart - 30
                 * @hooked woocommerce_template_single_meta - 40
                 * @hooked woocommerce_template_single_sharing - 50
                 * @hooked WC_Structured_Data::generate_product_data() - 60
                 */

                //do_action('woocommerce_single_product_summary');
                woocommerce_template_single_meta();
                woocommerce_template_single_title(); ?>
                <div class="product-detail-company__title">
                    Výrobca: <?php the_field('vyrobca'); ?>
                </div>

                <div class="product-detail-company__price product-detail-company__price--first">
                    <?php the_field('cena_nadpis_s_dph'); ?>

                </div>
                <span class="woocommerce-Price-amount amount woocommerce-Price-amount--first">
                    <?php the_field('cena_suma'); ?>
                </span>
                <div class="product-detail-company__price product-detail-company__price--second">

                    <?php the_field('cena_bez_dph_nadpis'); ?>
                    <?php woocommerce_template_single_price(); ?>
                </div>

                <?php

                woocommerce_template_single_add_to_cart();


                ?>

            </div>

        </div>




        <?php //do_action('woocommerce_after_single_product');
        ?>


    </div>
</div>


<div class="row product-detail__wrapper">
    <div class="  col-sm-12 col-md-6 col-lg-6">
        <?php //the_content();
        ?><div class="contact-form contact-form-desc">
            <div class="product-detail-box__info">
                <h1 class="product-detail-box__title">Popis produktu</h1>
                <p class="product-detail-box__content"> <?php the_field('produkt_popis'); ?></p>
            </div>
        </div>

    </div>
    <div class="product-detail-right col-sm-12 col-md-6 col-lg-6">
        <div class="contact-form contact-form-desc">
            <h5 class="contact-form__title contact-form__title--secondary">Opýtajte sa na tento produktu</h5>
            <?php echo do_shortcode('[contact-form-7 id="144" title="Contact form 2"]'); ?>
        </div>
    </div>
</div>

<div class="related-prod">
    <?php
    /**
     * Hook: woocommerce_after_single_product_summary.
     *
     * @hooked woocommerce_output_product_data_tabs - 10
     * @hooked woocommerce_upsell_display - 15
     * @hooked woocommerce_output_related_products - 20
     */
    //do_action('woocommerce_after_single_product_summary');
    woocommerce_output_product_data_tabs();
    woocommerce_output_related_products();


    ?>
</div>