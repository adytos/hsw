<?php
/*
Template Name: Services page
*/

get_header(); ?>
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$args = [
    'post_type' => 'sluzby',
    "posts_per_page" => -1,

    'order' => 'ASC'
];
$query = new WP_Query($args);
$active = 0;

while ($query->have_posts()) {
    $query->the_post();
    $service_url_slug = isset($_GET['service_id']) && !empty($_GET['service_id']) ? $_GET['service_id'] : '';
    $service_slug = sanitize_title(get_the_title());
    if ($service_url_slug && $service_url_slug == $service_slug) {
        $active = $query->current_post;
    }
}
?>
<section class="services support">
    <div class="support-page">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="support-page__title-wrap">
                        <h5 class="support-page__title"><?php wp_title('');
                                                        ?></h5>
                    </div>
                </div>
            </div>
            <?php
            if ($query->have_posts()) : ?>

                <div class="row">
                    <?php


                    $i = 0;

                    while ($query->have_posts()) : $query->the_post();
                        $current_service = false;
                        $service_slug = sanitize_title(get_the_title());

                        if ($active) {
                            if ($active == $query->current_post) {
                                $current_service = true;
                            }
                        }
                        if (!$current_service && !$active) {
                            if (!$i) {
                                $current_service = true;
                            }
                        }
                    ?>
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <div class="products-category__btn-wrap">
                                <button data-tab-id="<?php echo $service_slug; ?>" class="<?php echo $current_service ? 'products-category__btn--active' : '' ?> products-category__btn-js services-btn" href="<?php the_permalink(); ?>"><?php the_title();
                                                                                                                                                                                                                                            ?></button>
                            </div>
                        </div>
                        <?php wp_reset_postdata() ?>
                        <?php $i++; ?>
                    <?php

                    endwhile;

                    ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php


    if ($query->have_posts()) : ?>


        <div class="support-content">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php

                        $i = 0;


                        while ($query->have_posts()) : $query->the_post();
                            $current_service = false;
                            $service_slug = sanitize_title(get_the_title());
                            if ($active) {
                                if ($active == $query->current_post) {
                                    $current_service = true;
                                }
                            }
                            if (!$current_service && !$active) {
                                if (!$i) {
                                    $current_service = true;
                                }
                            }



                        ?>
                            <div class="support-content__box <?php echo $current_service ? 'support-content__box--active' : '' ?>" id="<?php echo $service_slug; ?>">
                                <img class="support__img" src="<?php echo get_field('img-service')['url']; ?>" alt="">
                                <h4 class="support-content__title"><?php the_title(); ?> </h5>
                                    <p class="support-content__article">
                                        <?php the_field('sluzba_clanok'); ?>

                                    </p>

                                    <h5 class="support-content__subtitle"> <?php the_field('title-service'); ?> </h5>
                                    <p class="support-content__article">
                                        <?php the_field('desc-service'); ?>
                                    </p>
                            </div>
                            <?php $i++; ?>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>


</section>

<?php get_footer(); ?>