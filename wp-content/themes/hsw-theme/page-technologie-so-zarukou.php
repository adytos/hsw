<?php get_header(); ?>


<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="technologies">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="technologies__title"> <?php the_title(); ?></h1>
                <div class="technologies__content">
                    <?php the_content();
                    the_archive_description();
                    ?>
                </div>
            </div>
        </div>
        <div class="row">

            <?php
            $term = $name;
            $terms = get_terms(array(
                'taxonomy' => 'category',
                'parent' => 99,
                'order' => 'ASC',
                'post_per_page' => get_queried_object()->$term_id,
            ));
            foreach ($terms as $key => $term) : {
            ?>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="technologies__btn-wrap">
                            <?php $term_link = get_term_link($term); ?>
                            <a class="technologies__btn" href="<?php echo get_term_link($term); ?>"><?php echo $term->name;
                                                                                                    ?></a>
                        </div>
                    </div>
                <?php  } ?>

            <?php endforeach;
            ?>
            <?php wp_reset_postdata() ?>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="category-line">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-3">
                <div class="technologie-widget__wrap">
                    <?php dynamic_sidebar('technologie-widget');
                    ?>
                </div>

            </div>

            <div class="col-9">
                <div class="row">
                    <?php

                    $args = [
                        'post_type' => 'technologie',
                        "posts_per_page" => -1,
                        'order' => 'ASC',

                    ];
                    $query = new WP_Query($args);

                    while ($query->have_posts()) : $query->the_post();
                    ?>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="technologies-box">
                                <div class="technologies-box__img-wrap">
                                    <a href="<?php the_permalink(); ?>"><img class="technologies-box__img" src="<?php echo get_field('technologia_obrazok')['url']; ?>"></a>

                                </div>
                                <div class="technologies-box__content">
                                    <h4 class="technologies-box__title"><?php the_title() ?></h4>
                                    <h5 class="technologies-box__desc"><?php the_field('tech_popis') ?></h5>
                                    <p class="technologies-box__year">Rok výroby: <?php the_field('technologia_rok') ?></p>

                                    <p class="technologies-box__price">Cena: <span class="technologies-box__price-span"> <?php the_field('technologia_cena');  ?> </span></p>
                                    <div class="technologies-box__detail-wrap">
                                        <a class="technologies-box__detail" href="<?php the_permalink(); ?>"> Detail produktu</a>
                                    </div>
                                    <div class="technologies-box__contact-wrap">
                                        <a class="technologies-box__contact" href=""> Kontaktovat predajcu </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php wp_reset_postdata() ?>
                    <?php

                    endwhile;

                    ?>

                </div>


            </div>


        </div>



    </div>
    </div>
    </div>
</section>


<?php get_footer(); ?>