<?php

/**
 * hsw-theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hsw-theme
 */

if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.0');
}

if (!function_exists('hsw_theme_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hsw_theme_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on hsw-theme, use a find and replace
		 * to change 'hsw-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('hsw-theme', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		add_theme_support('wc-product-gallery-lightbox');
		//add_theme_support('wc-product-gallery-zoom');
		//add_theme_support('wc-product-gallery-slider');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__('Primary', 'hsw-theme'),
				'menu-2' => __('Footer menu'),
			)
		);


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'hsw_theme_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'hsw_theme_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hsw_theme_content_width()
{
	$GLOBALS['content_width'] = apply_filters('hsw_theme_content_width', 640);
}
add_action('after_setup_theme', 'hsw_theme_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hsw_theme_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'hsw-theme'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'hsw-theme'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'hsw_theme_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function hsw_theme_scripts()
{
	//Jquery
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('hsw-theme-slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array("jquery"), _S_VERSION, true);
	//Styles
	wp_enqueue_style('hsw-theme-fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
	wp_enqueue_style('hsw-theme-slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
	wp_enqueue_style('hsw-theme-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('hsw-theme-style', 'rtl', 'replace');
	wp_enqueue_style('hsw-theme-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	//JS scripts
	wp_enqueue_script('hsw-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);
	wp_enqueue_script('hsw-theme-animations', get_template_directory_uri() . '/js/animations.js', array("jquery"), _S_VERSION, true);
	wp_enqueue_script('hsw-theme-services-tabs', get_template_directory_uri() . '/js/services-tabs.js', array("jquery"), _S_VERSION, true);
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'hsw_theme_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

//Excerpt length
function wp_example_excerpt_length($length)
{
	return 15;
}
add_filter('excerpt_length', 'wp_example_excerpt_length');

function new_excerpt_more($more)
{
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' 	=> 'Nastavenie témy',
		'menu_title'	=> 'Nastavenie témy',
		'menu_slug' 	=> 'theme-settings'
	));
}

//Create new post type in admin menu

add_action('init', 'create_post_type');

function create_post_type()
{
	register_post_type(
		'product',
		array(
			'labels' => array(
				'name' => __('Produkty'),
				'singular_name' => __('Produkty'),
				'add_new' => __('Pridať nový produkt'),
				'add_new_item' => __('Pridať nový produkt')
			),

			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-hammer',
			//'supports' => false,
			'menu_position' => 5,



		)
	);
	register_post_type(
		'sluzby',
		array(
			'labels' => array(
				'name' => __('Služby'),
				'singular_name' => __('Služby'),
				'add_new' => __('Pridať novú'),
				'add_new_item' => __('Pridať novú')
			),

			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-open-folder',
			//'supports' => false,
			'menu_position' => 8,
			'taxonomies'  => array('category'),

		)
	);
	register_post_type(
		'technologie',
		array(
			'labels' => array(
				'name' => __('Technologie'),
				'singular_name' => __('Technologie'),
				'add_new' => __('Pridať novú'),
				'add_new_item' => __('Pridať novú')
			),

			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-open-folder',
			//'supports' => false,
			'menu_position' => 8,
			'taxonomies'  => array('category'),

		)
	);
}


function ipdata_widgets_init()
{
	register_sidebar(array(
		'name'          => 'Technologie widget',
		'id'            => 'technologie-widget',
		'before_widget' => '<div class="chw-widget technologies-search">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="chw-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar(array(
		'name'          => 'Produkty widget',
		'id'            => 'produkty-widget',
		'before_widget' => '<div class="products-search">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="chw-title">',
		'after_title'   => '</h2>',
	));
}
add_action('widgets_init', 'ipdata_widgets_init');

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

function mytheme_add_woocommerce_support()
{
	add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'mytheme_add_woocommerce_support');


add_filter('wc_empty_cart_message', function ($text) {
	return 'Košik je prázdny.';
}, 10);

add_filter('woocommerce_loop_add_to_cart_link', 'replace_loop_add_to_cart_button', 10, 2);
function replace_loop_add_to_cart_button($button, $product)
{
	// Not needed for variable products
	if ($product->is_type('variable')) return $button;

	// Button text here
	$button_text = __("View product", "woocommerce");

	return '<a class="button button-detail-product" href="' . $product->get_permalink() . '">' . $button_text . '</a>';
}



add_action('user_register', 'atuser_info');
function atuser_info($user_id)
{
	global $wpdb;
	//$prefix=$wpdb->prefix;                
	$query_userinfo = "INSERT INTO user_info (id,fname,lname,address_1) VALUES      (" . $user_id . "," . $_POST['fname'] . "," . $_POST['lname'] . "," . $_POST['address_1_step2'] . ")";
	$wpdb->query($query_userinfo);
}


//Only show products in the front-end search results
function lw_search_filter_pages($query)
{
	if ($query->is_search) {
		$query->set('post_type', 'product');
		$query->set('wc_query', 'product_query');
	}
	return $query;
}

add_filter('pre_get_posts', 'lw_search_filter_pages');
