<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hsw-theme
 */

?>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="footer__img-wrap">
                    <img class="footer__img" src="<?php echo get_template_directory_uri() . '/images/LOGO/HSW-white-logo.svg' ?>" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="footer-box">
                    <h5 class="footer-box__title">Nakupovanie</h5>
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-2',
                            'menu_id'        => 'footer-menu',
                        )
                    );
                    ?>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="footer-box">
                    <h5 class="footer-box__title">Kontaktujte nás</h5>
                    <ul class="contact-items">
                        <li class="contact-item"><a class="contact-link" href="tel:<?php the_field('kontakt_mobil', 'option'); ?>"> <img class="contact-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/phone.svg' ?>" alt=""> <?php the_field('kontakt_mobil', 'option'); ?> </a></li>
                        <li class="contact-item"><a class="contact-link" href="mailto:<?php the_field('kontakt_mail', 'option'); ?>"> <img class="contact-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/mail.svg' ?>" alt=""> <?php the_field('kontakt_mail', 'option'); ?> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="footer-box">
                    <h5 class="footer-box__title">Predajňa osobný odber</h5>
                    <p class="footer-box__company"><?php the_field('kontakt_adresa', 'option') ?></p>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="footer-box">
                    <h5 class="footer-box__title">Sledujte nás</h5>
                    <ul class="social-items">
                        <li class="social-item"> <a class="social-link" href=""> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/fb.svg' ?>" alt=""></a> </li>
                        <li class="social-item"> <a class="social-link" href=""> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/ig.svg' ?>" alt=""></a> </li>
                        <li class="social-item"> <a class="social-link" href=""> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/linkedin.svg' ?>" alt=""></a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="footer__copy"> Copyright © 1996 - 2021 HSW Signall, s. r. o. </p>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>