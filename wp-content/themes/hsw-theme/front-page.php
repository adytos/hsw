<?php get_header(); ?>

<section class="main">
    <div class="container">
        <div class="row">
            <div class="main-content-order col-sm-12 col-md-12 col-lg-5">
                <div class="main-content">
                    <h1 class="main-box__title"> <?php the_field('main-title'); ?> </h1>
                    <p class="main-box__desc"> <?php the_field('main-subtitle'); ?> </p>
                    <div class="main-box__btn-wrap">
                        <a class="main-box__btn" href="<?php the_permalink(124); ?>"><?php the_field('main-btn'); ?></a>
                    </div>
                </div>
            </div>
            <div class="a col-sm-12 col-md-12 col-lg-7">
                <div class="main-js">
                    <?php
                    if (have_rows('main-slider')) :
                        while (have_rows('main-slider')) : the_row();
                    ?>
                            <div class="main-img__wrap">
                                <img class="main-img" src="<?php echo get_sub_field('main-slider-img')['url']; ?>" alt="">
                            </div>
                    <?php
                        endwhile;
                    endif;
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="category category-front">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="category__title-wrap">
                    <h1 class="category__title">Kategórie produktov - Eshop</h1>
                </div>
            </div>
            <?php
            $term = $name;
            $terms = get_terms(array(
                'taxonomy' => 'product_cat',
                'parent' => 0,
                'order' => 'ASC',

            ));
            foreach ($terms as $key => $term) {
            ?>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="category-box">
                        <div class="category-box__img-wrap">
                            <?php
                            $thumbnail_id = get_woocommerce_term_meta($term->term_id, 'thumbnail_id', true);
                            $image = wp_get_attachment_image_src($thumbnail_id, 'medium');

                            ?>
                            <img class="category-box__img" src="<?php echo $image[0] ?>" alt="">
                        </div>
                        <div class="category-box__btn-wrap">
                            <?php $term_link = get_term_link($term); ?>

                            <a class="category-box__btn" href="<?php echo get_term_link($term); ?>"><?php echo  $term->name; ?> <img class="category-box-arrow" src="<?php echo get_template_directory_uri() . '/images/icons/arrow-right2.svg' ?>" alt=""></a>
                        </div>
                    </div>
                </div>
            <?php  } ?>

            <?php //endforeach; 
            ?>
        </div>
    </div>
</section>
<section class="about">
    <div class="about-bcg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="about__title-wrap">
                        <h3 class="about__title"><?php the_field('about-title'); ?></h3>
                    </div>
                    <div class="about__content-wrap">
                        <p class="about__content">
                            <?php the_field('about-content'); ?>
                        </p>
                    </div>
                    <div class="about__btn-wrap">
                        <a class="about__btn" href="<?php the_permalink(66); ?>">Zisti o nás viac</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="features">
    <div class="container">
        <div class="row">
            <?php
            if (have_rows('features')) :
                while (have_rows('features')) : the_row();
            ?>
                    <div class="col-md-6 col-lg-3">
                        <div class="features-boxs">
                            <div class="features-box">
                                <div class="features-box__img-wrap">
                                    <img class="features-box__img" src="<?php echo get_sub_field('feature-img')['url']; ?>" alt="">
                                </div>
                                <h2 class="features-box__title"><?php the_sub_field('feature-title'); ?></h2>
                                <p class="features-box__content"><?php the_sub_field('feature-content'); ?></p>
                            </div>
                        </div>
                    </div>
            <?php
                endwhile;
            endif;
            ?>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="features-boxs-js">
                    <?php
                    if (have_rows('features')) :
                        while (have_rows('features')) : the_row();
                    ?>
                            <div class="features-boxs">
                                <div class="features-box__wrap">
                                    <div class="features-box">
                                        <div class="features-box__img-wrap">
                                            <img class="features-box__img" src="<?php echo get_sub_field('feature-img')['url']; ?>" alt="">
                                        </div>
                                        <h2 class="features-box__title"><?php the_sub_field('feature-title'); ?></h2>
                                        <p class="features-box__content"><?php the_sub_field('feature-content'); ?></p>
                                    </div>
                                </div>

                            </div>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="our-services">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="our-services__title-wrap">
                    <h1 class="our-services__title">Naše služby</h1>
                </div>
            </div>
        </div>

        <?php

        $args = [
            'post_type' => 'sluzby',
            "posts_per_page" => -1,
            'order' => 'ASC'
        ];

        $query = new WP_Query($args);
        $items = isset($query->posts) ? $query->posts :  false;
        if ($items) {
            $col = 0;
            $row = 0;
            $cols_per_row = 2;
            $rows = floor(count($items) / $cols_per_row);
            foreach ($items as $key => $item) {
                $image = get_field('img-service', $item->ID);
                $image = isset($image['url']) ? $image['url'] : '';
                $title = get_the_title($item->ID);
                $desc  = get_field('desc-service', $item->ID);
                $link = get_permalink(55) . '?service_id=' . sanitize_title($title);


                $html_img = '
                    <div class="our-services__img-wrap">
                        <img class="our-services__img" src=" ' . $image . ' " alt="">
                    </div>
                ';

                $html_reference =  '<div class="our-services__content">
                    <h1 class="our-services__headline"> ' . $title . '</h1>
                    <p class="our-services__desc"> ' . $desc . ' </p>
                    <div class="our-services__btn-wrap">
                        <a class="our-services__btn" href=" ' . $link . ' ">Zisti o tom viac</a>
                    </div>
                </div>';

                if ($col == 0) {
                    echo '<div class="row">';
                }

                if ($col != 0 && $col % $cols_per_row == 0) {
                    echo '<div class="row">';

                    $row++;
                }
                if ($row % $cols_per_row == 0) {

                    if ($col % $cols_per_row == 0) {
                        echo '<div class="col-12">';
                        echo '<div class="row">';
                        echo  '<div class="col-sm-12 col-md-12 col-lg-6 our-services__col-image-left">' . $html_img . '</div>';
                        echo '<div class="col-sm-12 col-md-12 col-lg-7 offset-lg-5 our-services__col-padding"  >' . $html_reference . '</div>';
                        echo '</div>';
                        echo '</div>';
                    } else {
                        echo '<div class="col-12">';
                        echo '<div class="row">';
                        echo '<div class="col-sm-12 col-md-12 col-lg-7 our-services__content-left our-services__col-padding"  >' . $html_reference . '</div>';
                        echo '<div class="col-sm-12 col-md-12 col-lg-6 our-services__col-image-right">' . $html_img . '</div>';
                        echo '</div>';
                        echo '</div>';
                    }
                } else {

                    if ($col % $cols_per_row == 0) {
                        echo '<div class="col-12">';
                        echo '<div class="row">';
                        echo '<div class="col-sm-12  col-md-12 col-lg-6 our-services__col-image-left">' . $html_img . '</div>';
                        echo '<div class="col-sm-12 col-md-12 col-lg-7 offset-lg-5 our-services__col-padding">' . $html_reference . '</div>';
                        echo '</div>';
                        echo '</div>';
                    } else {
                        echo '<div class="col-12">';
                        echo '<div class="row">';
                        echo '<div class="col-sm-12 col-md-12 col-lg-7 our-services__content-left our-services__col-padding">' . $html_reference . '</div>';
                        echo '<div class="col-sm-12  col-md-12 col-lg-6 our-services__col-image-right our-services__col-image-right-last">' . $html_img . '</div>';
                        echo '</div>';
                        echo '</div>';
                    }
                }

                if ($col % $cols_per_row != 0) {
                    echo '</div>';
                }

                if ($row == $rows) {
                    echo '</div>';
                }

                $col++;
            }
        }

        ?>
    </div>
</section>
<section class="news">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="news__title-wrap">
                    <h5 class="news__title">Aktuality</h5>
                </div>
            </div>
            <div class="col-12">
                <div class="blog-boxs-js">
                    <?php

                    $args = [
                        'post_type' => 'post',
                        "posts_per_page" => 3,
                        //'order' => 'ASC'
                    ];
                    $query = new WP_Query($args);

                    while ($query->have_posts()) : $query->the_post();
                    ?>

                        <div class="sing-blog">
                            <a class="blog-boxs__link" href="<?php the_permalink(); ?>">
                                <div class="blog__img-wrap">
                                    <?php
                                    the_post_thumbnail(); ?>
                                </div>
                                <div class="news-blog__content">
                                    <p class="blog__date">
                                        <?php the_date(); ?>
                                    </p>
                                    <div class="blog__title-wrap">
                                        <?php

                                        the_title('<h1 class="blog__title">', '</h1>');

                                        ?>
                                    </div>
                                    <div class="blog__text">
                                        <p> <?php if (is_page()) {
                                                the_excerpt();
                                            } ?>
                                        </p>
                                    </div>
                                    <div class="blog__button-wrap">
                                        <a class="blog__button" href="<?php the_permalink(); ?>">Čítaj celý článok</a>
                                    </div>
                                </div>
                            </a>
                        </div>


                        <?php wp_reset_postdata(); ?>
                    <?php

                    endwhile;

                    ?>
                </div>

            </div>

            <?php

            $args = [
                'post_type' => 'post',
                "posts_per_page" => 3,
                //'order' => 'ASC'
            ];
            $query = new WP_Query($args);

            while ($query->have_posts()) : $query->the_post();
            ?>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="blog-boxs">
                        <a class="blog-boxs__link" href="<?php the_permalink(); ?>">
                            <div class="sing-blog">
                                <div class="blog__img-wrap">
                                    <?php
                                    the_post_thumbnail(); ?>
                                </div>
                                <div class="news-blog__content">
                                    <p class="blog__date">
                                        <?php the_date(); ?>
                                    </p>
                                    <div class="blog__title-wrap">
                                        <?php

                                        the_title('<h1 class="blog__title">', '</h1>');

                                        ?>
                                    </div>
                                    <div class="blog__text">
                                        <p> <?php if (is_page()) {
                                                the_excerpt();
                                            } ?>
                                        </p>
                                    </div>
                                    <div class="blog__button-wrap">
                                        <a class="blog__button" href="<?php the_permalink(); ?>">Čítaj celý článok</a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <?php wp_reset_postdata() ?>
            <?php

            endwhile;

            ?>




            <div class="col-12">
                <div class="news__btn-wrap">
                    <a class="news__btn" href="<?php echo get_permalink(42); ?>">Zobraziť všetky aktuality</a>
                </div>
            </div>
        </div>
    </div>

</section>
<section class="partners">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="partners__title-wrap">
                    <h4 class="partners__title"><?php the_field('partners-title', '13'); ?></h4>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
            if (have_rows('partners')) :
                while (have_rows('partners')) : the_row();
            ?>
                    <div class="col-6 col-sm-4 col-md-4 col-lg-2">
                        <div class="partners-item__img-wrap">
                            <img class="partners-item__img" src="<?php echo get_sub_field('add-partner-img')['url']; ?>" alt="">
                        </div>
                    </div>
            <?php
                endwhile;
            endif;
            ?>

        </div>
    </div>
</section>
<section class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class=newsletter__wrap>
                    <img class="newsletter__img" src="<?php echo get_template_directory_uri() . '/images/icons/black-icon.svg' ?>" alt="">
                    <h5 class="newsletter__title"> <?php the_field('newsletter_nadpis'); ?></h5>
                    <p class="newsletter__subtitle">
                        <?php the_field('newsletter_popis'); ?>
                    </p>
                    <div class="newsletter__input-wrap">
                        <?php echo do_shortcode('[contact-form-7 id="195" title="Newsletter"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php get_footer(); ?>