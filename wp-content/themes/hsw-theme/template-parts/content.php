<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hsw-theme
 */

?>
<div class="sing-blog">
	<?php if (is_singular()) : ?>
		<div class="breadcrumb-section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="breadcrumb__wrap">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
								<li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_permalink(42); ?>">Aktuality</a></li>
								<li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	else : ?>

	<?php endif; ?>
	<div class="blog__img-wrap">
		<?php if (is_home()) {
			the_post_thumbnail();
		} ?>
	</div>


	<div class="blog__content">
		<div class="container">
			<div class="row">
				<div class="col">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php if (is_singular()) {
							the_post_thumbnail();
						}   ?>
						<p class="blog__date">
							<?php
							the_date();

							?>
						</p>

						<div class="blog__title-wrap">

							<?php
							//if ( is_singular( ) ) :
							the_title('<h1 class="blog__title">', '</h1>');
							//else :

							//endif;

							//if ( 'post' === get_post_type() ) :
							//
							?>


							<?php //endif; 
							?>
						</div>

						<div class="blog__text">
							<?php if (is_home()) {
								the_excerpt();
							} ?>

							<?php if (is_singular()) {


								if (is_singular()) {

									the_content(
										sprintf(
											wp_kses(
												/* translators: %s: Name of current post. Only visible to screen readers */
												__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'kpk-new-web'),
												array(
													'span' => array(
														'class' => array(),
													),
												)
											),
											wp_kses_post(get_the_title())
										)
									);

									wp_link_pages(
										array(
											'before' => '<div class="page-links">' . esc_html__('Pages:', 'kpk-new-web'),
											'after'  => '</div>',
										)
									);
								}
							}

							?>

						</div><!-- .entry-content -->
						<?php if (is_home()) { ?>
							<div class="blog__button-wrap">
								<a class="blog__button" href="<?php the_permalink(); ?>">Čítaj celý článok </a>
							</div>

						<?php } ?>
					</article><!-- #post-<?php the_ID(); ?> -->
				</div>
			</div>
		</div>
	</div>


</div>