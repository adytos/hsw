<?php get_header(); ?>
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="services support">
    <div class="support-page">

        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="support-page__title-wrap">
                        <h5 class="support-page__title"><?php wp_title(''); ?></h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php

                $args = [
                    'post_type' => 'sluzby',
                    "posts_per_page" => -1,

                    'order' => 'ASC'
                ];
                $query = new WP_Query($args);

                while ($query->have_posts()) : $query->the_post();
                ?>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="products-category__btn-wrap">
                            <a class="products-category__btn services-btn" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                    </div>
                <?php

                endwhile;

                ?>
            </div>
        </div>
    </div>
    <div class="support-content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="support-content__box">
                        <h4 class="support-content__title"><?php the_field('title-service'); ?> </h5>
                            <p class="support-content__article">
                                <?php the_field('clanok-1'); ?>
                            </p>
                            <img class="support__img" src="<?php echo get_field('img-service')['url']; ?>" alt="">
                            <h5> <?php the_field('support-subtitle'); ?> </h5>
                            <p class="support-content__article">
                                <?php the_field('desc-service'); ?>
                            </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php get_footer(); ?>