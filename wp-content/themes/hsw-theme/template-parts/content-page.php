<?php

/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hsw-theme
 */

?>

<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <?php if (is_page() || is_single()) { ?>
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(''); ?></a></li>

                        <?php } elseif (is_shop()) { ?>
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php the_permalink(124); ?>">Produkty</a></li>

                        <?php } else { ?>
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php the_permalink(124); ?>">Produkty</a></li>
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php the_permalink(); ?>"><?php single_term_title('');
                                                                                                                    ?></a></li>
                        <?php  } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="cat">
    <div class="container">
        <div class="row">
            <div class="col empty-col">

            </div>

        </div>
        <div class="row">

            <?php
            $term = $name;
            $terms = get_terms(array(
                'taxonomy' => 'product_cat',
                'order' => 'ASC',
                'parent' => get_queried_object()->$term_id,
            ));
            foreach ($terms as $key => $term) {
            ?> <?php if (is_single() || is_page()) { ?>

                <?php   } else { ?>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="products-category__btn-wrap">
                            <?php $term_link = get_term_link($term); ?>
                            <a class="products-category__btn" href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
                        </div>
                    </div>

                <?php   } ?>

            <?php  } ?>

            <?php //endforeach; 
            ?>
            <?php
            if (is_shop() || !is_woocommerce() || is_page() || is_single()) { ?>
                <div class="col-lg-12 woocommerce-products-wrapper">

                    <?php
                    woocommerce_content();

                    the_content();
                    ?>
                </div>


            <?php } else { ?>
                <div class="col-12">
                    <div class="cat-box">
                        <h5 class="cat-box__title"><?php single_cat_title();
                                                    ?></h5>
                        <div class="cat-box__content"><?php the_archive_description();
                                                        ?></div>
                    </div>
                    <?php the_content(); ?>
                </div>
                <div class="col-12">
                    <div class="category-line">

                    </div>
                </div>

                <?php if (!is_single()) { ?>
                    <div class="col-sm-12 col-md-12 col-lg-3">
                        <?php dynamic_sidebar('produkty-widget');
                        ?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-9 p-cat">
                        <?php

                        woocommerce_content();



                        ?>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-12 col-md-6 col-lg-12">
                        <?php

                        woocommerce_content();



                        ?>
                    </div>
                <?php } ?>

            <?php } ?>
        </div>
    </div>
</section>