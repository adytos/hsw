<?php get_header(); ?>
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="">Produkty</a></li>
                        <?php echo 'content product.php' ?>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="product-detail">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="product-detail__img-wrap">
                    <img src="" alt="">
                </div>
            </div>
            <div class="product-detail__content">
                <p><span>Kód produktu</span></p>
                <h5 class="product-detail__title"><?php the_title(); ?></h5>
                <p class="product-detail__madeby">Výrobca - </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php //the_content();
                ?>
                <div class="product-detail-box">
                    <h1 class="product-detail-box__title">Popis produktu</h1>
                    <p class="product-detail-box__content"></p>
                </div>
            </div>
            <div class="col-6">
                <div class="contact-form">
                    <h5 class="contact-form__title">Opýtajte sa na tento produktu</h5>
                    <?php echo do_shortcode('[contact-form-7 id="144" title="Contact form 2"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>