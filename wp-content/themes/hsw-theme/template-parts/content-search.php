<?php

/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hsw-theme
 */

?>
<div class="col-sm-12 col-md-6 col-lg-4">
	<article class="search-box" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">


			<?php if ('post' === get_post_type()) : ?>

			<?php endif; ?>
		</header><!-- .entry-header -->

		<?php hsw_theme_post_thumbnail(); ?>
		<?php the_title(sprintf('<h2 class="entry-title entry-title-search"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
		<div class="product-list__company">
			<?php the_field('vyrobca'); ?>
		</div>


		<?php woocommerce_template_single_price(); ?>


		<div class="search-button__wrap">
			<a class="search-button" href="<?php the_permalink(); ?>">Zobraziť produkt</a>
		</div>





	</article><!-- #post-<?php the_ID(); ?> -->
</div>