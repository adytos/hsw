<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hsw-theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header id="masthead" class="site-header">
        <div class="pre-header">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <ul cla>
                            <li class="social-item"> <a class="social-link" href="tel:<?php the_field('kontakt_mobil', 'option'); ?>"> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/phone-alt.svg' ?>" alt=""> <?php the_field('kontakt_mobil', 'option') ?> </a> </li>
                            <li class="social-item"> <a class="social-link" href="mailto:<?php the_field('kontakt_mail', 'option'); ?>"> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/zocial-email.svg' ?>" alt=""> <?php the_field('kontakt_mail', 'option') ?> </a> </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <p class="pre-header__desc">
                            <a href="<?php the_permalink(158); ?>">Veľkoobchod</a>
                        </p>

                    </div>
                    <div class="col-4">
                        <div class="header-social__wrap">
                            <ul class="header-socials">
                                <li class="social-item"> <a class="social-link" href=""> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/fb.svg' ?>" alt=""></a> </li>
                                <li class="social-item"> <a class="social-link" href=""> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/ig.svg' ?>" alt=""></a> </li>
                                <li class="social-item"> <a class="social-link" href=""> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/footer/linkedin.svg' ?>" alt=""></a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bar">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-sm-4 col-md-4 col-lg-2">
                        <a href="<?php echo get_home_url(); ?>"> <img class="header-logo" src="<?php echo get_template_directory_uri() . '/images/LOGO/HSW-logo.svg' ?>" alt=""></a>

                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <form class="header-search">
                            <img class="header-search-img" src="<?php echo get_template_directory_uri() . '/images/icons/header-zoom.svg' ?>">
                            <input name="s" class="header-input" type="text" placeholder="Zadajte produkt ktorý hľadáte">
                            <button type="submit" class="newsletter__buton header-btn">Hľadať</button>

                        </form>
                    </div>
                    <div class="col-lg-4">
                        <div class="user">
                            <div class="basket">
                                <a class="basket__title" href="<?php echo get_permalink(125); ?>">
                                    <img class="basket__img" src="<?php echo get_template_directory_uri() . '/images/icons/Shopping-cart.svg' ?>" alt="">
                                    Košík
                                </a>
                            </div>
                            <ul class="user-items">

                                <?php
                                if (is_user_logged_in()) { ?>
                                    <li class="user-item"> <a class="user-link user-link-log user-link-login" href="<?php the_permalink(127); ?>"> <img class="user-img" src="<?php echo get_template_directory_uri() . '/images/icons/ionic-md-person.svg' ?>" alt="">

                                            <?php
                                            $current_user = wp_get_current_user();

                                            printf(__('Ahoj %s', 'textdomain'), esc_html($current_user->user_login)) . '<br />';

                                            ?></a> </li>
                                <?php   } else { ?>
                                    <li class="user-item"> <a class="user-link user-link-log" href="<?php the_permalink(127); ?>"> <img class="user-img" src="<?php echo get_template_directory_uri() . '/images/icons/ionic-md-person.svg' ?>" alt=""> Prihlásenie </a> </li>
                                    <li class="user-item"> <a class="user-link user-link-reg" href="<?php the_permalink(127); ?>"> Registrácia</a></li>

                                <?php  } ?>
                            </ul>
                        </div>
                        <div class="mobile">
                            <a class="social-link mobile-link" href=""> <img class="social-img" src="<?php echo get_template_directory_uri() . '/images/icons/phone-alt.svg' ?>" alt=""> <?php the_field('kontakt_mobil', 'option') ?>
                            </a>
                            <a class="basket__title" href="<?php echo get_permalink(125); ?>">
                                <img class="basket__img basket__img-mobile" src="<?php echo get_template_directory_uri() . '/images/icons/Shopping-cart.svg' ?>" alt="">
                            </a>
                            <a class="user-link user-link-log user-link-mobile" href="<?php the_permalink(127); ?>"> <img class="user-img" src="<?php echo get_template_directory_uri() . '/images/icons/ionic-md-person.svg' ?>" alt=""></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav id="site-navigation" class="main-navigation">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e('', 'hsw-theme'); ?>
                            <span class="menu__btn-bar"></span>
                            <span class="menu__btn-bar"></span>
                            <span class="menu__btn-bar"></span>
                            <span class="menu__btn-bar"></span>
                        </button>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'menu-1',
                                'menu_id'        => 'primary-menu',
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>

        </nav>
    </header>