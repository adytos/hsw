(function($) {
  
      $('.main-js').slick({
        slidesToShow: 1,
        arrows: false,
        infinite:true,
        autoplay:true,
        dots:true,
        fade: true,
        cssEase: 'linear',
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1
            }
          }
        ]
      });

      $('.services-box-js').slick({
        slidesToShow: 1,
        arrows: false,
        infinite:true,
        autoplay:true,
        dots:false,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              infinite:true,
              autoplay:true,
              centerMode: true,
              slidesToShow: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1
            }
          }
        ]
      });

      $('.features-boxs-js').slick({
        slidesToShow: 4,
        arrows: false,
        infinite:true,
        autoplay:true,
        dots:true,
        fade: true,
        cssEase: 'linear',
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              infinite:true,
              autoplay:true,
              centerMode: true,
              slidesToShow: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1
            }
          }
        ]
      });

      $('.blog-boxs-js').slick({
        slidesToShow: 1,
        arrows: false,
        infinite:true,
        autoplay:true,
        dots:true,
        fade: true,
        cssEase: 'linear',
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              infinite:true,
              autoplay:true,
              centerMode: true,
              slidesToShow: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1
            }
          }
        ]
      });
  

    var html = $('html');
    var menuOpenClass = 'menu-opened';
    var menuBtn = $('.menu-toggle');

    function openMenu() {
      html.addClass(menuOpenClass);
    }
    
    function closeMenu() {
      html.removeClass(menuOpenClass);
    }
    
    menuBtn.on('click', function() {
      if( html.hasClass(menuOpenClass) ) {
        closeMenu();
      } else {
        openMenu();
      }
    });
  
  } ( jQuery ) );
  