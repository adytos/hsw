(function ($) {
  $(".products-category__btn-js").on("click", function (e) {
    e.preventDefault();

    $(".products-category__btn-js")
      .not(this)
      .removeClass("products-category__btn--active");

    $(this).addClass("products-category__btn--active");
    var tabId = $(this).data("tab-id");
    if (!tabId) {
      return;
    }
    var tabContent = $("#" + tabId);
    $(".support-content__box")
      .not(tabContent)
      .removeClass("support-content__box--active");
    tabContent.addClass("support-content__box--active");
  });

  var tabId = window.location.hash;
  $(tabId).click();
})(jQuery);
