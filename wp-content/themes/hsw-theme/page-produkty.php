<?php get_header(); ?>

<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="products">
    <div class="container">
        <div class="row">
            <?php
            $term = $name;
            $terms = get_terms(array(
                'taxonomy' => 'product_cat',

                'order' => 'ASC'
            ));
            foreach ($terms as $key => $term) {
            ?>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="products-category__btn-wrap">
                        <?php $term_link = get_term_link($term); ?>

                        <a class="products-category__btn" href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
                    </div>
                </div>
            <?php  } ?>

            <?php //endforeach; 
            ?>
        </div>
        <div class="row">

            <?php

            $args = [
                'post_type' => 'product',
                "posts_per_page" => -1,

                'order' => 'ASC'
            ];
            $query = new WP_Query($args);

            while ($query->have_posts()) : $query->the_post();
            ?>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="products-box">
                        <div class="products-box__img-wrap">
                            <img class="products-box__img" src="<?php echo get_the_post_thumbnail_url(); ?>?>" alt="">
                        </div>
                        <div class="products-box__content">
                            <h1 class="products-box__title"><?php the_title() ?></h1>
                            <p class="products-box__subtitle">Dobre dosky </p>
                            <?php $price_html = $product->get_price_html() ?>
                            <h4 class="products-box__price"> <?php echo $price_html   ?></h4>
                            <div class="products-box__btn-wrap">
                                <a class="products-box__btn" href="<?php the_permalink(); ?>">Detail produkt</a>
                            </div>
                            <div class="products-box__available-wrap">
                                <p class="products-box__available"><?php echo $product->get_stock_quantity(); ?></p>
                            </div>

                        </div>
                    </div>
                </div>
            <?php

            endwhile;

            if (function_exists('wp_paginate')) :
                wp_paginate();

            endif;

            ?>

        </div>
    </div>

</section>
<?php get_footer(); ?>