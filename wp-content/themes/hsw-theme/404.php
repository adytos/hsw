<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package hsw-theme
 */

get_header();
?>
<section>
    <div class=container>
        <div class="row">
            <div class="col">
                <h5 class="not-found-title">Ľutujeme, stránka sa nenašla.</h5>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
