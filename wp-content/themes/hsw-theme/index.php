<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hsw-theme
 */

get_header();
?>

<section class="blog-posts">
	<div class="breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb__wrap">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
							<li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php wp_title(''); ?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="blog-post-items">
		<div class="container">
			<div class="row">
				<?php
				if (have_posts()) :
					/* Start the Loop */
					while (have_posts()) :
						the_post();
				?>
						<div class="col-sm-12 col-md-6 col-lg-4">
							<a class="blog-boxs__link" href="<?php the_permalink(); ?>">
								<?php get_template_part('template-parts/content', get_post_type()); ?>
							</a>
						</div>
					<?php
					endwhile;

				else : ?>
					<?php
					get_template_part('template-parts/content', 'none');
					?>
				<?php
				endif;
				?>
			</div>
		</div>
	</div>
</section>

<?php

get_footer();
