<?php get_header(); ?>

<section class="contact">
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb__wrap">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php wp_title(''); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-map">
        <div class="mapouter">
            <div class="gmap_canvas"><iframe width="auto" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=ko%C5%A1ice%20hsw%20signall&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://embedgooglemap.net/maps/76"></a><br>
                <style>
                    .mapouter {
                        position: relative;
                        text-align: right;
                        height: 500px;
                        width: auto;
                    }
                </style><a href="https://www.embedgooglemap.net"></a>
                <style>
                    .gmap_canvas {
                        overflow: hidden;
                        background: none !important;
                        height: 500px;
                        width: auto;
                    }
                </style>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-5">
                <div class="contact-info">
                    <h5 class="contact-info__title">Kontaktné informácie</h5>
                    <ul class="contact-info__items">
                        <li class="contact-info__item"><a class="contact-info__link" href="tel:<?php the_field('kontakt_mobil', 'option'); ?>"> <img class="contact-info__img" src="<?php echo get_template_directory_uri() . '/images/icons/contact/contact-phone.svg' ?>" alt=""> <?php the_field('kontakt_mobil', 'option') ?> </a></li>
                        <li class="contact-info__item"><a class="contact-info__link" href="mailto: <?php the_field('kontakt_mail', 'option'); ?>"> <img class="contact-info__img" src="<?php echo get_template_directory_uri() . '/images/icons/contact/contact-email.svg' ?>" alt=""> <?php the_field('kontakt_mail', 'option') ?> </a></li>
                        <li class="contact-info__item"><a class="contact-info__link"> <img class="contact-info__img" src="<?php echo get_template_directory_uri() . '/images/icons/contact/contact-place.svg' ?>" alt=""> <?php the_field('kontakt_adresa', 'option') ?> </a></li>
                    </ul>
                    <ul>
                        <li class="contact-info__item">IČO: <?php the_field('ico', 'option') ?></li>
                        <li class="contact-info__item">DIČ: <?php the_field('dic', 'option') ?></li>

                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-1">
                <div class="contact-form">
                    <h5 class="contact-form__title">Napíšte nám</h5>
                    <?php echo do_shortcode('[contact-form-7 id="91" title="Contact form 1"]'); ?>
                </div>
            </div>
        </div>
    </div>


</section>

<?php get_footer(); ?>