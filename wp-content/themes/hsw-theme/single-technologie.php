<?php get_header(); ?>

<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="">Technológie</a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="technology-detail">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="technology-detail__img-wrap">
                    <?php $image = get_field('technologia_obrazok'); ?>

                    <img class="technology-detail__img" src="<?php echo $image['url'] ?>" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="technology-detail__title-wrap">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div>
                        <h1 class="technology-detail__title"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="technology-detail__content">
                    <p class="technology-detail__content-title">Detaily o stroji</p>
                    <p class="technology-detail__subtitle"><?php the_field('tech_popis'); ?> </p>
                    <ul class="technology-detail__items">


                        <li class="technology-detail__item">Pracovná plocha: <span class="technology-detail__item-span"> <?php the_field('technologia_rozmery'); ?></span></li>
                        <li class="technology-detail__item">Rok výroby: <span class="technology-detail__item-span"><?php the_field('technologia_rok'); ?></span></li>
                        <li class="technology-detail__item">Inkousty: <span class="technology-detail__item-span"><?php the_field('inkousty'); ?></span> </li>
                        <li class="technology-detail__item">Stav: <span class="technology-detail__item-span"><?php the_field('technologia_stav'); ?></span></li>
                    </ul>
                    <p class="technology-detail__price-box"> Cena:<span class="technology-detail__price"> <?php the_field('technologia_cena') ?></span> <span class="technology-detail__tax"> bez DPH</span> </p>
                    <ul class="technology-detail__items">
                        <li class="technology-detail__contact">Kontakt: <?php the_field('kontaktna_osoba'); ?></li>
                        <li class="technology-detail__phone">Tel: <?php the_field('kontakt_na_predajcu'); ?></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="contact-form contact-form__technologie">
                    <h5 class="contact-form__title">Opýtajte sa na tento produktu</h5>
                    <?php echo do_shortcode('[contact-form-7 id="144" title="Contact form 2"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>