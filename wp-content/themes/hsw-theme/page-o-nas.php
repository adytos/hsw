<?php get_header(); ?>


<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb__wrap">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/icons/home-icon.svg' ?>" alt=""></a></li>
                        <li class="breadcrumb-item"><a class="breadcrumb-link" href=""><?php the_title(); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="support">
    <div class="support-page">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="support-page__title-wrap">
                        <h5 class="support-page__title"><?php wp_title(''); ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="about-content__box">
                        <h4 class="about-content__title"><?php the_field('about-title'); ?> </h5>
                            <p class="about-content__article">
                                <?php the_field('about-article-1'); ?>

                            </p>
                            <img class="about__img" src="<?php echo get_field('about-img-1')['url']; ?>" alt="">
                            <h5> <?php the_field('about-subtitle'); ?> </h5>
                            <p class="about-content__article">
                                <?php the_field('about-article-2'); ?>
                            </p>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>

<?php get_footer(); ?>